<!DOCTYPE html>
<html>
<head>
    
    
    <style type='text/css'>
        body {
            background-color: #dbdbdb;
        }
        .bg-success{
            background-color: #165b2a;            
            
        }
        
        .text-center{
            padding-top: 30px;
            padding-bottom: 30px;
            text-align: center ;
            
        }
        .fixed-top {
            position: relative;
            margin-top: 4px;
            top:-22px;
            
        }
        .text-secondary {
            color: #6c757d;
        }
        .text-light {
            color: #f8f9fa;
        }
        .text-justify {
            text-align: justify;
        }
        .modal {
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            z-index: 1050;
            margin-top: 70px;
            margin-bottom: 70px;
            margin-left: 70px;
            margin-right: 70px;
            overflow: h 0;
            box-shadow: 40px 40px 40px grey;
        }
        .modal-content {
            background-color: white;
        }
        .content{
            margin-left:70px;
            margin-right:70px;
        }
        .ml{
            margin-left:30px;            
        }
        
        hr{
            box-sizing: content-box;
            height: 0;
            overflow: visible;
            margin-top: 1rem;
            margin-bottom: 1rem;
            border: 0;
            border-top: 1px solid rgba(0, 0, 0, 0.1);
        }
        .row {
            display: -ms-flexbox;
            display: flex;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            margin-right: -15px;
            margin-left: -15px;
        }
        .form-group {
            margin-bottom: 1rem;
            display: -ms-flexbox;
            display: flex;
            -ms-flex: 0 0 auto;
            flex: 0 0 auto;
            -ms-flex-flow: row wrap;
            flex-flow: row wrap;
            -ms-flex-align: center;
            align-items: center;
            margin-bottom: 0;
        }
        .form-control{
            padding: 0.375rem 0.75rem;
            font-size: 1rem;
            line-height: 1.5;
            color: #495057;
            border: 1px solid #ced4da;
            border-radius: 0.25rem;
            transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
            
        }
        .col-6 {
            -ms-flex: 0 0 50%;
            flex: 0 0 50%;            
            position: relative;
            
        }
        .col-4 {
            -ms-flex: 0 0 33.333333%;
            flex: 0 0 33.333333%;
            max-width: 33.333333%;
        }
        .font-weight-bold {
            font-weight: 700 !important;
        }
        
    </style>
</head>
<body>
    <div class='modal modal-content'>

        <div class='bg-success fixed-top'>            
            <h1 class='text-light text-center'>SOLICITAÇÃO DE ORCAMENTO</h1>            
        </div>
        <div class='content text-justify'>
            <div>
                <h2 class='text-secondary'>INFORMAÇÕES DO CLIENTE</h2>
                <hr>
                <div class='ml'>
                    <div class='row'>
                        <div class='form-group col-6'>
                            <label class='font-weight-bold text-secondary'>Nome</label>
                            <input class='form-control ml' value='nome' >
                        </div>
                        <div class='form-group col-6'>
                            <label class='font-weight-bold text-secondary'>Sobrenome</label>
                            <input class='form-control ml' value='sobrenome' >
                        </div>
                    </div>
                    <div class='row'>
                        <div class='form-group col-6'>
                            <label class='font-weight-bold text-secondary'>E-mail</label>
                            <input class='form-control ml' value='email' >
                        </div>                    
                        <div class='form-group col-6'>
                            <label class='font-weight-bold text-secondary'>Celular/Whatsapp</label>
                            <input class='form-control ml' value='celular' >
                        </div>                    
                    </div>
                    
                    <div class='row'>
                        <div class='form-group col-12'>
                            <label class='font-weight-bold text-secondary'>CEP</label>
                            <input class='form-control ml' value='cep' >
                        </div>
                    </div>
                    <div class='row'>
                        <div class='form-group col-6'>
                            <label class='font-weight-bold text-secondary'>Endereço</label>
                            <input class='form-control ml' value='endereco' >
                        </div>
                        <div class='form-group col-6'>
                            <label class='font-weight-bold text-secondary'>Número</label>
                            <input class='form-control ml' value='numero' >
                        </div>
                    </div>
                    <div class='row'>
                        <div class='form-group col-6'>
                            <label class='font-weight-bold text-secondary'>Bairro</label>
                            <input class='form-control ml' value='bairro' >
                        </div>
                        <div class='form-group col-6'>
                            <label class='font-weight-bold text-secondary'>Estado</label>
                            <input class='form-control ml' value='estado'>                   
                        </div>
                    </div>
                    <div class='row'>
                        <div class='form-group col-12'>
                            <label class='font-weight-bold text-secondary'>Complemento</label>
                            <input class='form-control ml' value='complemento' >
                        </div>
                    </div>
                    <div class='row'>
                        <div class='col-6'>
                            <label class='font-weight-bold  text-secondary'>Data de entrega que o cliente precisa</label>
                            <input class='form-control ml' value='data-de-entrega' >                       
                        </div>
                    </div>
                </div>
            </div>
            
            <div>
                <h2 class='text-secondary'>INFORMAÇÕES DO PRODUTO</h2>
                <hr>
                <div class='ml'>
                    <div class='row'>
                        <div class='col-6'>
                            <label class='font-weight-bold  text-secondary'>Tipo do Copo</label>
                            <input class='form-control ml' id='tipo-do-copo' value='tipo-do-copo' />
                        </div>                        
                        <div class='col-6'>
                            <label class='font-weight-bold  text-secondary'>Cor do Copo</label>
                            <input class='form-control ml' id='cor-do-copo' value='cor-do-copo' />
                        </div>
                    </div>
                    <div class='row'>
                        <div class='form-group col-6'>
                            <label class='font-weight-bold  text-secondary'>Cor/cores de Impressão</label>
                            <input class='form-control' value='colors'>                        
                            <label class='font-weight-bold ml text-secondary'>Cor/cores</label>
                            <input class='form-control' value='cores'>
                        </div>
                        <div class='form-group col-4'>
                            <label class='font-weight-bold  text-secondary'>Quantidade</label>
                            <input class='form-control' value='quantidade'>
                        </div>
                        
                    </div>
                    
                    
                    <div class='row'>
                        <div class='form-group col-6'>
                            <label class='font-weight-bold  text-secondary'>Precisa do serviço de criação de arte</label>                            
                            <input class='form-control' value='equipe-design'>                            
                        </div>
                        <div class='form-group col-6'>
                            <label class='font-weight-bold  text-secondary'>Incluir porta copos</label>                            
                            <input class='form-control' value='porta-copos'>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
            
    </div>
</body>
</html>


