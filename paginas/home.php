<div class="container-fluid">
    <div class="row mt-5">
        <div class="col-12">
            <img class="img-fluid rounded mx-auto d-block" src="assets/img/home/banner.png" alt=""/>
        </div>
    </div>
    <br>
    <div class="container marketing mt-5">

        <!-- Three columns of text below the carousel -->
        <!-- <div class="row">
            <div class="col text-center">
                <img class="rounded-circle" src="assets/img/home/oque-oferecemos.jpg" alt="Generic placeholder image" width="140" height="140">
                <h2>O QUE OFERECEMOS</h2>
                <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies vehicula ut id elit. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent commodo cursus magna.</p>

            </div> -->
            <!-- /.col-lg-4 -->
            <!-- <div class="col text-center">
                <img class="rounded-circle text-center" src="assets/img/home/por-que-fazemos.jpg" alt="Generic placeholder image" width="140" height="140">
                <h2>POR QUE FAZEMOS</h2>
                <p>Somos uma empresa de impacto sócio ambiental .  Acreditamos que juntos somos todos parte da solução. Para isso apresentamos diferentes soluções em sustentabilidade  gerar uma mudança de comportamento que trará benefícios diretos ao meio ambiente.</p>

            </div> -->
            <!-- /.col-lg-4 -->
            <!-- <div class="col text-center">
                <img class="rounded-circle" src="assets/img/home/para-quem-fazemos.jpg" alt="Generic placeholder image" width="140" height="140">
                <h2>PARA QUEM FAZEMOS</h2>
                <p>Nossos produtos e serviços são destinados a todos que se preocupam não só com a parte financeira mas também com o meio ambiente. São inúmeras as diferentes ocasiões de consumo do nosso Capim Ecopo e derivados. Desde Eventos esportivos,  corporativos, noitada, boite, escolas, festivais shows e até escolas.</p>

            </div> -->
            <!-- /.col-lg-4 -->
        <!-- </div> -->
        <!-- /.row -->
        <div class="row mt-5">
            <div class="col-">
                <img class="img-fluid rounded mx-auto d-block" src="assets/img/home/site-construcao.png" alt=""/>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-md-7">
                <h2 class="featurette-heading">Nossa Missão</h2>
                <p class="lead">
                    Somos o jeito novo de fazer eventos, o jeito novo de se divertir<br>
                    Somos a pegada permanente que resiste em se manter consciente<br>
                    Somos amantes da terra, de festa, de gente<br>
                    Somos parte de voce que ama viver<br>
                    Somos o simples, o essencial, e o que nasce independente do local<br>
                    Somos todos parte da solução :) ♻<br>                    
                </p>
            </div>
            <div class="col-md-5">
                <img class="featurette-image img-fluid mx-auto" src="assets/img/home/copos_produtos_home.png" alt="">
                <div class="d-flex flex-row justify-content-center">
                    <a href="?p=produtos"><button class="btn btn-success mt-2" type="button"> Conheça nossos produtos </button></a>
                </div>
            </div>
        </div>

        <div class="row mt-5">
            <div class="col-12">
                <img class="featurette-image img-fluid mx-auto" src="assets/img/home/descartaveis-01.png" alt="Generic placeholder image">
            </div>
            
        </div>
        <br>
        <div class="row mt-5">
            <div class="col-md-3">
                <div class="text-center">
                    <h4 class="text-success">Mapa do site</h4>
                    <img src="assets/img/home/folhinha.png" alt="Generic placeholder image">
                    <div class="lead"><?php require "templates/navbarmapa.php"; ?></div>
                </div>
            </div><!-- /.col-lg-4 -->
            <div class="col-md-4">
                <!-- <div class="text-center">
                    <h4 class="text-success">Fique por dentro com a gente</h4>
                    <img src="assets/img/home/folhinha.png" alt="Generic placeholder image">
                    <p class="lead mt-3">Seja parte da solução você também!</p>
                    <form action="" method="post">
                        <div class="form-group col-8 offset-2">
                            <input class="form-control" type="text" name="nome" id="nome" placeholder="Seu Nome">
                            <input class="form-control mt-2" type="email" name="email" id="email" placeholder="E-mail">
                            <button class="btn btn-success mt-3" type="submit">Cadastrar</button>
                        </div>
                    </form>
                </div> -->
            </div>
            <!-- /.col-lg-4 -->
            <div class="col-md-5">
                <div class="text-center">
                    <h4 class="text-success">Nos acompanhe nas redes sociais</h4>
                    <img src="assets/img/home/folhinha.png" alt="Generic placeholder image">
                    <div class="row text-center mt-4">
                        <div class="col-4 offset-2">
                            <a href="https://www.instagram.com/capimecocopo/" target="_blank"><img src="assets/img/home/instagram_icon.png" alt="Capim Instagram"></a>
                        </div>
                        <div class="col-4">
                            <a href="https://facebook.com.br/CapimSeloVerde" target="_blank"><img src="assets/img/home/facebook_icon.png" alt="Capim Facebook"></a>
                        </div>
                    </div>
                    <div class="fb-page mt-3" data-href="https://www.facebook.com/capim-selo-verde" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/capim-selo-verde" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/capim-selo-verde">Capim SeloVerde</a></blockquote></div>
                
                </div>            

            </div><!-- /.col-lg-4 -->
        </div>
    </div><!-- /.container -->
</div>