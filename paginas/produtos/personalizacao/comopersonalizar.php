<div class="container">
    <div class="alert bg-success text-light text-center" role="alert">
        <h2 class="font-weight-bold">TUTORIAL DE PERSONALIZAÇÃO</h2>        
    </div>
    <div class="row">
        <div class="col-6">
            <div class="form-row mt-3 bg-capim">
                <div class="form-group">
                    <p class="font-weight-bold h4 text-light mt-3 ml-3">Arte do Copo</p>
                </div>
            </div>
            <div class="form-row mt-2 ml-1">
                <p class="text-justify">A criação de sua arte é de sua responsabilidade.</p>
                <p class="text-justify">Caso haja necessidade de nossa equipe criar a arte, entre em contato através de <b>contato@capimseloverde.com.br</b> com o briefing e enviaremos o orçamento com base na complexidade.</p>
            </div>
        </div>

        <div class="col-6 ml-auto">
            <div class="form-row mt-3 bg-capim">
                <div class="form-group">
                    <p class="font-weight-bold h4 text-light mt-3 ml-3">Formatos de Arquivo</p>
                </div>
            </div>
            <div class="form-row mt-2 ml-1">
                <p class="text-justify">Recebemos arquivos (apenas vetorizados) nos seguintes formatos <b>.AI .EPS .PDF</b> as fontes da arte devem ser convertidas em curvas antes do envio.</p>                
            </div>
            <div class="form-row text-center">
                <div class="form-group col-4">
                    <img src="assets/img/personalizacao/formato_ai.png" alt="">
                </div>
                <div class="form-group col-4">
                    <img src="assets/img/personalizacao/formato_eps.png" alt="">
                </div>
                <div class="form-group col-4">
                    <img src="assets/img/personalizacao/formato_pdf.png" alt="">
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-6">
            <div class="form-row ml-1 mt-3">
                <p class="h1 text-secondary">1. COR DO COPO</p>
                <p class="text-justify">Opaco não é possivel visualizar o que tem dentro do #Copim, já o Translúcido e Neon é possível ver o que tem dentro do #Copim fosco e Translúcido não é transparente. Possibilita a Visualização de líquidos no interior do copo.</p>
            </div>
        </div>
        <div class="col-6 ml-auto">
            <div class="form-row">
                <img src="assets/img/personalizacao/fotos_copos.png" alt="">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-6">
            <div class="form-row ml-1 mt-3">
                <p class="h1 text-secondary">2. COR DE IMPRESSÃO</p>
                <p class="text-justify">Envie as cores selecionadas no código Pantone. Quando os códigos não são enviados, nos fazemos o processo de identificação e você aprova. As cores visualizadas no monitor diferem parcialmente das cores da arte impressa.</p>
            </div>
        </div>
        <div class="col-6 ml-auto">
            <div class="form-row">
                <img src="assets/img/personalizacao/exemplo_cores.png" alt="">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-6">
            <div class="form-group mt-3">
                <p class="h1 text-secondary">3. GABARITO</p>
                <label class="text-justify">Faça o Download dos Gabaritos:</label>
                <ul class="md-1">
                
                    <li><a href="assets/files/gabaritos/CAPIMECOCOPO_350ml_GABARITO.pdf" download >Eco Copo 350ml</a></li>
                    <li><a href="assets/files/gabaritos/CAPIMECOCOPO_400ml_GABARITO.pdf" download >Eco Copo 400ml</a></li>
                    <li><a href="assets/files/gabaritos/CAPIMECOCOPO_550ml_GABARITO.pdf" download >Eco Copo 550ml</a></li>
                    <li><a href="assets/files/gabaritos/CORDINHA_GABARITO.pdf" download >Cordinha</a></li>
                </ul>
            </div>
        </div>
        <div class="col-6 ml-auto">
            <div class="form-row">
                <img src="assets/img/personalizacao/area_personalizacao.png" alt="">                
            </div>
        </div>
    </div>
    <div class="row">
        <div class="form-row bg-capim col-12">
            <p class="text-center font-weight-bold h4 text-light mt-3 mb-3">Observações</p>            
        </div>
        <div class="form-row ml-1">
            <div class="form-row mt-2">
                <p class="text-justify"><b>Documento de Aprovação de Arte:</b><br>
                Será enviada para você conferir se a arte final está correta, Não esqueã de verificar os textos e números presentes na arte estão corretos</p>
            </div>
            <div class="form-row">
                <p class="text-justify"><b>Alterações:</b><br>
                Se após finalizada e enviada, o cliente julgar necessário fazer alguma alteração na arte e, ele mesmo não puder fazê-la, a meu copo eco concede sem custo adicionais 02 alterações. Ao ultrapassar o limite de solicitações (02), serão cobrados R$50,00 por alteração extra.</p>                
            </div>
        </div>
    </div>
    <div class="alert bg-success text-light text-center" role="alert">
        <h2 class="font-weight-bold">FLUXO DE PERSONALIZAÇÃO</h2>
    </div>
    <p class="mt-2 text-center">FLUXOGRAMA DE PEDIDO DO SEU #CapimEcoCopo PERSONALIZADO!</p>
    <div class="row mt-5">
        <div class="text-center">
            <img src="assets/img/personalizacao/exemplo_fluxo_pedidos.png" alt="">
        </div>
    </div>
</div>