<div class="container">
    <div class="alert bg-success text-light text-center" role="alert">
        <h2 class="font-weight-bold">PRODUTOS</h2>
    </div>
    <div class="row">
        <div class="col-6">
            <a href="?p=ecocopo550ml">
                <img class="img-fluid" src="assets/img/produtos/produtos_ecocopo550ml.png" alt="Ecocopo 550ml">
                <p class="lead text-center">Ver mais detalhes</p>
            </a>
        </div>
        <!-- <div class="col-6">
            <a href="?p=ecocopo500ml">
                <img class="img-fluid" src="assets/img/produtos/produtos_ecocopo500ml.png" alt="Ecocopo 500ml">
                <p class="lead text-center">Ver mais detalhes</p>
            </a>
        </div> -->
        <div class="col-6">
            <a href="?p=ecocopo400ml">
                <img class="img-fluid" src="assets/img/produtos/produtos_ecocopo400ml.png" alt="Ecocopo 400ml">
                <p class="lead text-center">Ver mais detalhes</p>
            </a>
        </div>
    </div>
    <div class="row">
        <div class="col-6">
            <a href="?p=ecocopo350ml">
                <img class="img-fluid" src="assets/img/produtos/produtos_ecocopo350ml.png" alt="Ecocopo 350ml">
                <p class="lead text-center">Ver mais detalhes</p>
            </a>
        </div>
        <div class="col-6">
            <!-- <a href="?p=portacopos">
                <img class="img-fluid" src="assets/img/produtos/produtos_porta_copos.png" alt="Porta Copos">
                <p class="lead text-center">Ver mais detalhes</p>
            </a> -->
        </div>
    </div>
    <!-- <div class="row">
        <div class="col-6">
            <a href="?p=longdrink330ml">
                <img class="img-fluid" src="assets/img/produtos/produtos_longdrink330ml.png" alt="Longdrink 330ml">
                <p class="lead text-center">Ver mais detalhes</p>
            </a>
        </div>
        <div class="col-6">
            <a href="?p=champanhe">
                <img class="img-fluid" src="assets/img/produtos/produtos_taca_champanhe.png" alt="Taça Champanhe">
                <p class="lead text-center">Ver mais detalhes</p>
            </a>
        </div>
    </div> -->
    <div class="row">
        <!-- <div class="col-6">
            <a href="?p=gin">
                <img class="img-fluid" src="assets/img/produtos/produtos_taca_gin.png" alt="Taça Gin">
                <p class="lead text-center">Ver mais detalhes</p>
            </a>
        </div> -->
    </div>    
</div>