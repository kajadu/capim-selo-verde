<script src="assets/js/navprodutos.js" type="text/javascript"></script>
<div class="container">
    <div class="alert bg-success text-light text-center" role="alert">
        <h2 class="font-weight-bold">CAPIM ECOCOPO 550ML</h2>
    </div>

    <div class="row">
        <div class="col-12">
            <img class="img-fluid head-produto" src="assets/img/produtos/tipos/head_ecocopo550ml.png" alt="Ecocopo 550ml">            
        </div>        
    </div>

    <div class="alert bg-success text-light text-center mt-4" role="alert">
        <h3 class="font-weight-bold">OPACOS</h3>        
    </div>
    <div class="row">
        <?php
        $cores_opacos = ['opaco_amarelo','opaco_azul','opaco_branco','opaco_preto','opaco_verde','opaco_vermelho'];
        foreach ($cores_opacos as $n => $cor_opaco):   
        $item = $n+1;                        
        ?>  
        <div class="col-2">
            <img class="produto-cor" src="assets/img/exemplo-copos/550ml/<?php echo $cor_opaco ?>.jpg">
            <p class="text-center lead"><?php echo str_replace("opaco_","",$cor_opaco);?></p> 
        </div>  
        <?php endforeach; ?>
    </div>

    <div class="alert bg-success text-light text-center mt-4" role="alert">
        <h3 class="font-weight-bold">TRANSLUCIDOS</h3>
    </div>
    <div class="row">
    <?php
        $cores_translucidos = ['translucido_agua','translucido_amarelo_neon','translucido_azul','translucido_laranja_neon','translucido_natural','translucido_rosa_neon','translucido_roxo','translucido_verde_neon'];
        foreach ($cores_translucidos as $n => $cor_translucido):   
        $item = $n+1;                
        ?>
        <div class="col-2">    
            <img class="produto-cor" src="assets/img/exemplo-copos/550ml/<?php echo $cor_translucido ?>.jpg">
            <p class="text-center lead"><?php echo str_replace("_"," ",str_replace("translucido_","",$cor_translucido));?></p>
        </div>            
        <?php endforeach; ?>
    </div>
    <input type="hidden" id="page" value="<?php echo $_REQUEST['p']?>">
    <nav class="nav nav-tabs nav-justified">
        <a id="descricao-<?php echo $_REQUEST['p']?>" class="nav-item nav-link">Descrição</a>
        <a id="gabarito-<?php echo $_REQUEST['p']?>" class="nav-item nav-link">Gabarito Personalização</a>
        <a id="coresopacas-<?php echo $_REQUEST['p']?>" class="nav-item nav-link">Cores Opacas</a>
        <a id="corestranslucidas-<?php echo $_REQUEST['p']?>" class="nav-item nav-link">Cores Translucidas</a>
    </nav>
    
    <div class="item mt-4" id="item-descricao-<?php echo $_REQUEST['p']?>">
        <p class="lead">
            ECOCOPO<br>
            Capacidade: 550 ml
        </p>
        <ul>
            <li>Polipropileno</li>
            <li>BPA FREE</li>
            <li>100 % reutilizável</li>
            <li>Térmico</li>
            <li>Made in Brasil</li>
            <li>Seu reuso evita em média 4 copos descartáveis de uso único por pessoa , ao dia.</li>
            <li>Segundo estudos europeus é até 25 mais sustentável do que os descartáveis.</li>
        </ul>
    </div>
    <div class="item mt-4" id="item-gabarito-<?php echo $_REQUEST['p']?>">
        <p class="lead">
            Baixe o gabarito <a href="assets/files/gabaritos/CAPIMECOCOPO_550ml_GABARITO.pdf" download >aqui</a><br>
            • Arte em<br>
            01 cor de impressão<br>
            02 cores de impressão<br>
            Veja como <a href="?p=comopersonalizar">personalizar</a><br>
            
        </p>
    </div>
    <div class="item mt-4" id="item-coresopacas-<?php echo $_REQUEST['p']?>">
        <p class="lead">
        - OPACOS<br>
            Preto<br>
            Branco<br>
            Verde<br>
            Azul <br>
            Amarelo<br>
            Vermelho<br>

            
        </p>
    </div>
    <div class="item mt-4" id="item-corestranslucidas-<?php echo $_REQUEST['p']?>">
         <p class="lead">
         - TRANSLUCIDOS<br>
            Amarelo Neon<br>
            Verde Neon<br>
            Rosa Neon<br>
            Laranja Neon<br>
            Natural<br>
            Azul translucido<br>
            Roxo translucido<br>
            Verde agua translucido<br>
        </p>
    </div>
    
    
</div>