<?php
    $copo = isset($_GET['copo']) ? $_GET['copo'] : '';
?>
<div class="container">
    <div class="row">
        <div class="col">
            <button id="button-compra" class="btn btn-menu form-control" >COMPRA</button>
        </div>
        <div class="col">
            <button id="button-emprestimo" class="btn btn-menu form-control" >EMPRESTIMO</button>
        </div>
    </div>
</div>

<div id="orcamento" class="container">

</div>

<div id="compra-copos" class="container">
    <input type="hidden" id="tipo-copo" value="<?php echo $copo ?>">

</div>

<div id="emprestimo-copos" class="container">

</div>

