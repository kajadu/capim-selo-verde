<script src="assets/js/orcamentos.js"></script>
<div class="bg-light">
    
    <form class="ml-3" id="form-orcamentos" name="form-orcamentos" >
        <input type="hidden" id="page" name="page" value="350ml">
        <div class="row">

            <div class="col">
                <h1 class="text-secondary" >Cores</h1>

                <label class="text-secondary" >Opacos</label>

                <div class="row ml-1">
                    <div id="button-vermelho" class="ml-1 button button-vermelho" value="opaco_vermelho"><img src="assets/img/buttons.png" alt=""/></div>
                    <div id="button-verde" class="ml-1 button button-verde" value="opaco_verde"><img src="assets/img/buttons.png" alt=""/></div>
                    <div id="button-roxo" class="ml-1 button button-roxo" value="0"><img src="assets/img/buttons.png" alt=""/></div>
                    <div id="button-tiffany" class="ml-1 button button-tiffany" value="0"><img src="assets/img/buttons.png" alt=""/></div>
                    <div id="button-preto" class="ml-1 button button-preto" value="0"><img src="assets/img/buttons.png" alt=""/></div>
                    <div id="button-branco" class="ml-1 button button-branco" value="0"><img src="assets/img/buttons.png" alt=""/></div>
                    <div id="button-azul" class="ml-1 button button-azul" value="opaco_azul"><img src="assets/img/buttons.png" alt=""/></div>
                    <div id="button-amarelo" class="ml-1 button button-amarelo" value="0"><img src="assets/img/buttons.png" alt=""/></div>
                    <div id="button-rosa" class="ml-1 button button-rosa-neon" value="opaco_rosa"><img src="assets/img/buttons.png" alt=""/></div>
                    <div id="button-turquesa" class="ml-1 button button-turquesa" value="0"><img src="assets/img/buttons.png" alt=""/></div>
                    <div id="button-laranja" class="ml-1 button button-laranja-neon" value="opaco_laranja"><img src="assets/img/buttons.png" alt=""/></div>
                </div> 
                <br>
                <label class="text-secondary" >Translucidos</label>

                <div class="row ml-1">
                    <div id="button-natural" class="ml-1 button button-natural" value="translucido_natural"><img src="assets/img/buttons.png" alt=""/></div>
                    <div id="button-verde-neon" class="ml-1 button button-verde-neon" value="0"><img src="assets/img/buttons.png" alt=""/></div>
                    <div id="button-amarelo-neon" class="ml-1 button button-amarelo-neon" value="0"><img src="assets/img/buttons.png" alt=""/></div>
                    <div id="button-rosa-neon" class="ml-1 button button-rosa-neon" value="0"><img src="assets/img/buttons.png" alt=""/></div>
                    <div id="button-agua" class="ml-1 button button-agua" value="0"><img src="assets/img/buttons.png" alt=""/></div>
                    <div id="button-azul-translucido" class="ml-1 button button-azul-escuro" value="0"><img src="assets/img/buttons.png" alt=""/></div>
                    <div id="button-laranja-neon" class="ml-1 button button-laranja-neon" value="0"><img src="assets/img/buttons.png" alt=""/></div>
                    <div id="button-roxo-translucido" class="ml-1 button button-roxo" value="0"><img src="assets/img/buttons.png" alt=""/></div>
                </div>
                <br>
                <div class="form-group">
                    <label for="coresnaarte"><h1 class="text-secondary">Cor de Impressão *</h1></label><br>
                    <!-- <div class="ml-1 form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="colors" id="colors" value="1">
                        <label class="form-check-label" for="inlineRadio1">01 Cor</label>
                    </div>
                    <div class="ml-1 form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="colors" id="colors" value="2">
                        <label class="form-check-label" for="inlineRadio2">02 Cores</label>
                    </div> -->
                    <input class="form-control" id="cores" name="cores" placeholder="Digite qual será a cor de personalização da sua arte." oblig="1">
                </div>
                
                <div class="row">
                    <div class="form-group col-12">
                        <label for="quantidade"><h1 class="text-secondary" >Quantidade</h1></label>
                    </div>
                    <div class="form-group col-12">
                        <select class="ml-1 form-control" id="quantidade" name="quantidade">
                            <?php
                            $i = 100;
                            while ($i <= 3000) :
                                ?>
    
                                <option value="<?= $i; ?>" > <?= $i; ?> unid.</option>                                                              
                                <?php $i = $i + 100; ?>
                            <?php endwhile; ?>
                        </select>
                        <!-- <input type="text" id="outras_quantidades" name="outras_quantidades" class="ml-3 form-control" placeholder="Outras (MIN 100unid.)"> -->
                    </div>
                </div>
                <!-- <div class="form-group">
                    <label for="opcoesdeenvio"><h1 class="text-secondary" >Opções de envio</h1></label><br>
                    <div class="ml-1 form-check form-check-inline">
                        <input class="form-check-input" type="radio" id="opcoes_de_envio" name="opcoes_de_envio" value="retirada">
                        <label class="form-check-label text-secondary" for="inlineRadio1">Retirada</label>
                    </div>
                    <div class="ml-1 form-check form-check-inline">
                        <input class="form-check-input" type="radio" id="opcoes_de_envio" name="opcoes_de_envio" value="transportadora">
                        <label class="form-check-label text-secondary" for="inlineRadio2">Transportadora</label>
                    </div>
                </div> -->
                <div class="row">
                    <div class="form-group col-12">
                        <label for="nome" class="text-secondary">Nome *</label>
                        <input class="form-control" id="nome" name="nome" placeholder="Nome" oblig="1">
                    </div>                    
                </div>
                <div class="row">
                    <div class="form-group col-6">
                        <label for="nome" class="text-secondary">E-mail *</label>
                        <input class="form-control" id="email" name="email" placeholder="email@email.com.br" oblig="1">
                    </div>
                    <div class="form-group col-6">
                        <label for="nome" class="text-secondary">Confirme Seu E-mail *</label>
                        <input class="form-control" id="confirma-email" name="confirma-email" placeholder="email@email.com.br" oblig="1">
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-6">
                        <label for="nome" class="text-secondary">Celular/Whatsapp *</label>
                        <input class="form-control" id="celular" name="celular" placeholder="(00) 0 0000-0000" oblig="1">
                    </div>                    
                </div>
                <div class="row">
                    <div class="form-group col-12">
                        <label for="cep" class="text-secondary">CEP *</label>
                        <input class="form-control" id="cep" name="cep" placeholder="00000-000" oblig="1">
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-6">
                        <label for="endereco" class="text-secondary">Endereço *</label>
                        <input class="form-control" id="endereco" name="endereco" oblig="1">
                    </div>
                    <div class="form-group col-6">
                        <label for="numero" class="text-secondary">Número *</label>
                        <input class="form-control" id="numero" name="numero" oblig="1">
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-6">
                        <label for="bairro" class="text-secondary">Bairro *</label>
                        <input class="form-control" id="bairro" name="bairro" oblig="1">
                    </div>
                    <div class="form-group col-6">
                        <label for="estado" class="text-secondary">Estado *</label>
                        <input class="form-control" id="estado" name="estado" oblig="1">                   
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-12">
                        <label for="complemento" class="text-secondary">Complemento</label>
                        <input class="form-control" id="complemento" name="complemento" >
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <label for="data-de-entrega" class="text-secondary">Para quando precisa do seu Capim Eco Copo entregue? *</label>
                        <input class="form-control" id="datepicker" name="data-de-entrega" oblig="1">                       
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="form-group col-12">
                        <h5 class="text-secondary font-weight-bold">Já possui a arte de seu capim ecocopo pronta?</h5>                        
                        <div class="input-group mb-3">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="anexar" name="anexar">
                                <label class="custom-file-label" id="label-anexar" for="anexar"></label>
                            </div>
                        </div>
                        
                        <p class="text-secondary" >Clique <a href="?p=comopersonalizar" target="_blank">AQUI</a> para ver como personaliza sua arte.</p>
                    </div>
                </div>

                <!-- <div class="row mt-4">
                    <div class="form-group col-12">
                        <h5 class="text-secondary font-weight-bold">Já possui a arte de seu capim ecocopo pronta?</h5>                        
                        
                        <div class="ml-1 form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="arte-pronta" id="arte-pronta" value="S">
                            <label class="form-check-label" for="inlineRadio1">Sim</label>
                        </div>
                        <div class="ml-1 form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="arte-pronta" id="arte-pronta" value="N">
                            <label class="form-check-label" for="inlineRadio2">Não</label>
                        </div>
                        
                        <p class="text-secondary" >Clique <a href="?p=comopersonalizar" target="_blank">AQUI</a> para ver como personaliza sua arte.</p>
                    </div>
                </div> -->

                <div class="row mt-2">
                    <div class="form-group col-12">
                        <h5 class="text-secondary font-weight-bold">Caso ainda não tenha arte pronta, não se preocupe.<br> Temos uma equipe de designers e nossa equipe de atendimento entrará em contato para atender melhor sua necessidade.</h5>
                        <div class=" form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" name="equipe-design" id="equipe-design">
                            <label class="form-check-label text-secondary" for="equipe-design">Preciso do serviço de criação de arte</label>
                        </div>
                    </div>
                </div>

                <div class="row mt-2">
                    <div class="form-group col-12">
                        <h5 class="text-secondary font-weight-bold">Gostaria de incluir em seu orçamento informações dos nossos porta-copos + cordinha personalizada?</h5>
                        <div class="ml-1 form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="porta-copos" id="porta-copos" value="S">
                            <label class="form-check-label" for="inlineRadio1">Sim</label>
                        </div>
                        <div class="ml-1 form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="porta-copos" id="porta-copos" value="N">
                            <label class="form-check-label" for="inlineRadio2">Não</label>
                        </div>
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="form-group col-12">
                        <h5 class="text-secondary font-weight-bold">Deseja fazer algum comentário complementar no seu pedido? Digite abaixo:</h5>
                        <textarea class="form-control" id="comentarios" name="comentarios" cols="70" rows="10"></textarea>
                    </div>
                </div>
                
                <div class="row">
                    <div class="form-group col-12">
                        <button type="button" id="solicitar-orcamento" name="solicitar-orcamento" class="btn btn-success mt-4">Solicitar Orçamento</button>
                    </div>                       
                </div>
                
            </div>

            <div class="col ml-3">
                <div class="row">
                    <div class="form-group col-12">
                
                        <h1 class="text-secondary" >Exemplo:</h1>                        
                        <div id="imagem-copo"></div>
                        <input type="hidden" id="cor-do-copo" name="cor-do-copo" value="" />
                        <input type="hidden" id="tipo-do-copo" name="tipo-do-copo" value="" />
                    </div>
                </div>
                
            </div>

        </div>
    </form>        

</div>