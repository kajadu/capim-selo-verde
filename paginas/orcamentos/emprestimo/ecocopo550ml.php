<script src="assets/js/orcamentos.js"></script>
<div class="bg-light">
    <!-- id="form-orcamentos" name="form-orcamentos" -->
    <form class="ml-3" id="form-orcamentos" name="form-orcamentos" >
        <div class="row">

            <div class="col">
                <h1 class="text-secondary" >Cores</h1>

                <label class="text-secondary" >Opacos</label>

                <div class="row ml-1">
                    <div id="button-vermelho" class="ml-1 button-vermelho"><img src="assets/img/buttons.png" alt=""/></div>
                    <div id="button-verde" class="ml-1 button-verde"><img src="assets/img/buttons.png" alt=""/></div>
                    <div id="button-roxo" class="ml-1 button-roxo"><img src="assets/img/buttons.png" alt=""/></div>
                    <div id="button-tiffany" class="ml-1 button-tiffany"><img src="assets/img/buttons.png" alt=""/></div>
                    <div id="button-preto" class="ml-1 button-preto"><img src="assets/img/buttons.png" alt=""/></div>
                    <div id="button-branco" class="ml-1 button-branco"><img src="assets/img/buttons.png" alt=""/></div>
                    <div id="button-azul" class="ml-1 button-azul"><img src="assets/img/buttons.png" alt=""/></div>
                    <div id="button-amarelo" class="ml-1 button-amarelo"><img src="assets/img/buttons.png" alt=""/></div>
                </div> 
                <br>
                <label class="text-secondary" >Translucidos</label>

                <div class="row ml-1">
                    <div id="button-natural" class="ml-1 button-natural"><img src="assets/img/buttons.png" alt=""/></div>
                    <div id="button-verde-neon" class="ml-1 button-verde-neon"><img src="assets/img/buttons.png" alt=""/></div>
                    <div id="button-amarelo-neon" class="ml-1 button-amarelo-neon"><img src="assets/img/buttons.png" alt=""/></div>
                    <div id="button-rosa-neon" class="ml-1 button-rosa-neon"><img src="assets/img/buttons.png" alt=""/></div>
                    <div id="button-agua" class="ml-1 button-agua"><img src="assets/img/buttons.png" alt=""/></div>
                    <div id="button-azul-escuro" class="ml-1 button-azul-escuro"><img src="assets/img/buttons.png" alt=""/></div>
                    <div id="button-azul-claro" class="ml-1 button-azul-claro"><img src="assets/img/buttons.png" alt=""/></div>
                    <div id="button-laranja-neon" class="ml-1 button-laranja-neon"><img src="assets/img/buttons.png" alt=""/></div>
                </div>
                <br>
                <div class="row">
                    <div class="form-group">
                        <label for="quantidade"><h1 class="text-secondary" >Quantidade</h1></label><br>
                    </div>
                </div>
                <div class="form-inline">
                    <select class="ml-1 form-control" id="quantidade" name="quantidade">
                        <?php
                        $i = 100;
                        while ($i <= 3000) :
                            ?>

                            <option value="<?= $i; ?>" > <?= $i; ?> unid.</option>                                                              
                            <?php $i = $i + 100; ?>
                        <?php endwhile; ?>
                    </select>
                    <input type="text" id="outras_quantidades" name="outras_quantidades" class="ml-3 form-control" placeholder="Outras (MIN 100unid.)">
                </div>
                <div class="form-group">
                    <label for="coresnaarte"><h1 class="text-secondary">Cores na arte</h1></label><br>
                    <div class="ml-1 form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="colors" id="colors" value="1">
                        <label class="form-check-label" for="inlineRadio1">1</label>
                    </div>
                    <div class="ml-1 form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="colors" id="colors" value="2">
                        <label class="form-check-label" for="inlineRadio2">2</label>
                    </div>
                </div>

                <div class="form-group">
                    <label for="opcoesdeenvio"><h1 class="text-secondary" >Opções de envio</h1></label><br>
                    <div class="ml-1 form-check form-check-inline">
                        <input class="form-check-input" type="radio" id="opcoes_de_envio" name="opcoes_de_envio" value="retirada">
                        <label class="form-check-label text-secondary" for="inlineRadio1">Retirada</label>
                    </div>
                    <div class="ml-1 form-check form-check-inline">
                        <input class="form-check-input" type="radio" id="opcoes_de_envio" name="opcoes_de_envio" value="transportadora">
                        <label class="form-check-label text-secondary" for="inlineRadio2">Transportadora</label>
                    </div>
                </div>

                <div class="form-group">
                    <label for="CEP" class="text-secondary">Informe o CEP para calculo do Frete</label>
                    <input class="form-control" id="cep" name="cep" placeholder="00000-000">                    
                </div>
                <div class="form-group">
                    <label for="nome" class="text-secondary">Nome</label>
                    <input class="form-control" id="nome" name="nome" placeholder="Nome Completo">
                </div>
                <div class="form-group">
                    <label for="nome" class="text-secondary">Email</label>
                    <input class="form-control" id="email" name="email" placeholder="Exemplo: email@email.com.br">
                </div>
                
                <button type="button" id="solicitar-orcamento" name="solicitar-orcamento" class="btn btn-success">Solicitar Orçamento</button><br><br>
            </div>

            <div class="col">
                <div class="row">
                    <h1 class="text-secondary" >Exemplo:</h1><br>
                </div>
                <div class="row">
                    <div id="imagem-copo"></div>
                    <input type="hidden" id="cor-do-copo" name="cor-do-copo" value="" />
                </div>
                <div class="row mt-4">
                    <img src="assets/img/area_impressao_550ml.png" alt=""/>
                </div>
                
                <div class="row mt-4">
                    <button type="button" id="download-gabarito" name="download-gabarito" class="btn btn-success">Download Gabarito</button><br>
                </div>
                <div class="row">
                    <label class="text-secondary" for="download-gabarito">Confira o tutorial de personalização.</label>
                </div>
            </div>

        </div>
    </form>        

</div>

