<script src="assets/js/emprestimos.js"></script>
<div class="bg-light">    
    <!-- id="form-orcamentos" name="form-orcamentos" -->
    <form class="ml-3" id="form-emprestimo" name="form-orcamentos">
        <div class="row">

            <div class="col">
                <p class="lead mt-3">
                    Não quer investir no capim eco copo personalizado ? Não tem problema, peque  emprestado conosco! 😊<br>
                    Funciona assim: <br>
                    Você retira os copos conosco de maneira   consignada (formalizando de acordo com a quantidade necessária) e, faz um evento mais sustentável evitando centenas de copos descartáveis de uso único!  ♻🌱. <br>
                    Ao final de sua festa ou evento,  os copos que por acaso você não devolver para nosso estoque , será cobrado o valor de R$ 3,50 por unidade . <br>
                    Os que você nos devolver (em perfeito estado), será cobrado apenas 0,15 centavos por capim eco copo,  valor necessário apenas para que possamos fazer a higienização dos mesmos,  em nossa máquina especial (de alta temperatura, baixo consumo de água e energia) para que outras pessoas possam utilizar também! 🌱♻<br>
                    Se preferir, poderá retirar e entregar os copos em nosso escritório, sem custo de frete.<br>
                    Mas se desejar poderemos orçar o valor de entrega, de acordo com sua localidade... 
                </p>
                    
                <div class="row">
                    <div class="form-group col-12">
                        <label for="quantidade" class="text-secondary">Quantidade</label>
                        <input type="text" id="quantidade" name="quantidade" class="form-control" placeholder="100 unidades">
                    
                        <!-- <select class="ml-1 form-control" id="quantidade" name="quantidade">
                            <?php
                            $i = 100;
                            while ($i <= 3000) :
                                ?>
    
                                <option value="<?= $i; ?>" > <?= $i; ?> unid.</option>                                                              
                                <?php $i = $i + 100; ?>
                            <?php endwhile; ?>
                        </select> -->
                        <!-- <input type="text" id="outras_quantidades" name="outras_quantidades" class="ml-3 form-control" placeholder="Outras (MIN 100unid.)"> -->
                    </div>
                </div>
                <!-- <div class="form-group">
                    <label for="opcoesdeenvio"><h1 class="text-secondary" >Opções de envio</h1></label><br>
                    <div class="ml-1 form-check form-check-inline">
                        <input class="form-check-input" type="radio" id="opcoes_de_envio" name="opcoes_de_envio" value="retirada">
                        <label class="form-check-label text-secondary" for="inlineRadio1">Retirada</label>
                    </div>
                    <div class="ml-1 form-check form-check-inline">
                        <input class="form-check-input" type="radio" id="opcoes_de_envio" name="opcoes_de_envio" value="transportadora">
                        <label class="form-check-label text-secondary" for="inlineRadio2">Transportadora</label>
                    </div>
                </div> -->
                <div class="row">
                    <div class="form-group col-12">
                        <label for="nome" class="text-secondary">Nome *</label>
                        <input class="form-control" id="nome" name="nome" placeholder="Nome" oblig="1">
                    </div>                    
                </div>
                <div class="row">
                    <div class="form-group col-6">
                        <label for="nome" class="text-secondary">E-mail *</label>
                        <input class="form-control" id="email" name="email" placeholder="email@email.com.br" oblig="1">
                    </div>
                    <div class="form-group col-6">
                        <label for="nome" class="text-secondary">Confirme Seu e-mail *</label>
                        <input class="form-control" id="confirma-email" name="confirma-email" placeholder="email@email.com.br" oblig="1">
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-6">
                        <label for="nome" class="text-secondary">Celular/Whatsapp *</label>
                        <input class="form-control" id="celular" name="celular" placeholder="(00) 0 0000-0000" oblig="1">
                    </div>                    
                </div>
                <div class="row">
                    <div class="form-group col-12">
                        <label for="cep" class="text-secondary">CEP</label>
                        <input class="form-control" id="cep" name="cep" placeholder="00000-000">
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-6">
                        <label for="endereco" class="text-secondary">Endereço</label>
                        <input class="form-control" id="endereco" name="endereco" >
                    </div>
                    <div class="form-group col-6">
                        <label for="numero" class="text-secondary">Número</label>
                        <input class="form-control" id="numero" name="numero" >
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-6">
                        <label for="bairro" class="text-secondary">Bairro</label>
                        <input class="form-control" id="bairro" name="bairro" >
                    </div>
                    <div class="form-group col-6">
                        <label for="estado" class="text-secondary">Estado</label>
                        <input class="form-control" id="estado" name="estado">                   
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-12">
                        <label for="complemento" class="text-secondary">Complemento</label>
                        <input class="form-control" id="complemento" name="complemento" >
                    </div>
                </div>
                <!-- <div class="row">
                    <div class="col-6">
                        <label for="data-de-entrega" class="text-secondary">Para quando precisa do seu Capim Eco Copo entregue? *</label>
                        <input class="form-control" id="datepicker" name="data-de-entrega" oblig="1">                       
                    </div>
                </div> -->
                <!-- <div class="row mt-4">
                    <div class="form-group col-12">
                        <h5 class="text-secondary font-weight-bold">Já possui a arte de seu capim ecocopo pronta?</h5>                        
                        <button type="button" class="btn btn-success" >Anexar Arquivo</button>
                        <div class="input-group mb-3">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="anexar" name="anexar">
                                <label class="custom-file-label" id="label-anexar" for="anexar"></label>
                            </div>
                        </div>
                        
                        <p class="text-secondary" >Clique <a href="?p=comopersonalizar" target="_blank">AQUI</a> para ver como personaliza sua arte.</p>
                    </div>
                </div> -->
                
                <!-- <div class="row mt-2">
                    <div class="form-group col-12">
                        <h5 class="text-secondary font-weight-bold">Gostaria de incluir em seu orçamento informações dos nossos porta-copos + cordinha personalizada?</h5>
                        <div class="ml-1 form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="porta-copos" id="porta-copos" value="S">
                            <label class="form-check-label" for="inlineRadio1">Sim</label>
                        </div>
                        <div class="ml-1 form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="porta-copos" id="porta-copos" value="N">
                            <label class="form-check-label" for="inlineRadio2">Não</label>
                        </div>
                    </div>
                </div> -->
                <!-- <div class="row mt-2">
                    <div class="form-group col-12">
                        <h5 class="text-secondary font-weight-bold">Deseja fazer algum comentário complementar no seu pedido? Digite abaixo:</h5>
                        <textarea class="form-control" id="comentarios" name="comentarios" cols="70" rows="10"></textarea>
                    </div>
                </div> -->
                
                <div class="row">
                    <div class="form-group col-12">
                        <button type="button" id="solicitar-emprestimo" name="solicitar-emprestimo" class="btn btn-success mt-4">Solicitar Orçamento</button>
                    </div>                       
                </div>
                
            </div>

            <div class="col ml-3">
                <div class="row">
                    <div class="form-group col-12">
                
                        <!-- <h1 class="text-secondary" >Exemplo:</h1> -->
                        
                        <img class='imagem-copo-emprestimo mt-3' src="assets/img/orcamentos/emprestimo/emprestimo-copos.png">                        

                    </div>
                </div>
                
            </div>

        </div>
    </form>        

</div>