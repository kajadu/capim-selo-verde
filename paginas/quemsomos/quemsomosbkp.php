<div class="container">

    <div class="alert bg-success text-light text-center" role="alert">
        <h2 class="font-weight-bold">QUEM SOMOS</h2> 
    </div>

    <img class="img-fluid" src="assets/img/quemsomos/quemsomos_01.png" alt="">
    <div class="row mt-5">
        <div class="col-5">
            <img class="img-fluid" src="assets/img/quemsomos/quemsomos_02.png" alt="">
            <p class="lead">
                SOMOS O JEITO NOVO DE FAZER EVENTOS, O JEITO NOVO DE SE DIVERTIR<br>
                SOMOS A PEGADA PERMANENTE QUE RESISTE EM SE MANTER CONSCIENTE<br>
                SOMOS AMANTES DA TERRA,<br>
                DE FESTA, DE GENTE<br>
                SOMOS PARTE DE VOCE QUE AMA VIVER<br>
                SOMOS O SIMPLES, O ESSENCIAL, E O QUE NASCE INDEPENDENTE DO LOCAL<br>
                NOS COMOS CAPIM SELO VERDE. <br>
                Somos todos parte da solução!
            </p>
        </div>
        
        <div class="col-6 offset-1">
            <img class="img-fluid" src="assets/img/quemsomos/quemsomos_03.png" alt="">
        </div>
    </div>
</div>