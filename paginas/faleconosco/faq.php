<?php
$faq = [
    "Qual a quantidade mínima?"=>"100 Copos.",
    "Qual o tempo de produção?"=>"Prazo médio de 5 dias após arte enviada, aprovada e, pagamento confirmado.",
    "Forma de pagamento?"=>"Boleto, depósito bancário e cartão de crédito.",

    "É possível fazer um pedido com mais de uma arte?"=>"Sim, porém cada pedido será calculado como um pedido separado, para escala de valores e número de cores de impressão. Pois, cada arte demanda um procedimento exclusivo no processo de personalização.",
    "O que é um arquivo em Vetor? Se precisar, vocês vetorizam?"=>"Arquivo vetorizado é quando podemos mexer nele sem perder a qualidade e as propriedades da arte, como fontes, formatos etc. Se necessitar da vetorização será cobrado um valor de acordo com complexidade da imagem.",
    "É possível fazer troca de cor de copo para uma mesma arte?"=>"Sim. Você poderá escolher diferentes cores de copo, para uma mesma arte. Porém para uma quantidade mínima de 200 copos de cada cor. Para mudanças de cores, de quantidades menores para uma mesma arte inferior a 200 copos, terá um custo extra de R$50,00 - por alteração de cor de copo.",

    "Quem é responsável pelo frete?"=>"O transporte , dependendo da localidade , é passado de forma personalizada (recebendo informações de CEP e endereço completo ) e com orçamento a parte.",
    "Vocês personalizam outros materiais como Taça, Bituqueira.... ?"=>"Sim. Mande sua demanda para o email contato@capimseloverde.com.br para que possamos entender qual sua demanda e qual solução se encaixa perfeitamente ao seu caso.",
    "Quais as cores de copo?"=>"Confira as cores na página de <a href='?p=orcamentos'>orçamentos.</a>",
    "Porque o Capim EcoCopo é mais ecológico?"=>"Utilizamos, no processo de fabricação, o versátil material denominado polipropileno. Este material é de alta qualidade, bastante produto. Segundo alguns estudos na Europa, chega a ser até 25 vezes mais sustentável que os copos descartáveis de uso único. Sem contar o processo educacional da mudança de comportamento na lógica de consumo, pois nos possibilita anular o consumo do copo descartável em qualquer situação e disseminar boas práticas socioambientais. Dessa forma, levando uma cultura mais sustentável para o dia a dia das pessoas - além de ser um copo que melhora diretamente a sua saúde, já que não libera substâncias tóxicas e cancerígenas quando, por exemplo, recebe líquidos quentes. E, também, o copo pode ser congelado."
];
?>
<div class="container">
    <div class="alert bg-success text-light text-center" role="alert">
        <h2 class="font-weight-bold">PERGUNTAS FREQUENTES</h2>
    </div>
    <div class="accordion" id="accordion">
        <?php 
        $itens=1;
        foreach($faq as $pergunta=>$resposta):        
        ?>
            <div class="card">
                <div class="card-header" id="item<?=$itens?>" type="button" data-toggle="collapse" data-target="#collapse<?=$itens?>" aria-expanded="false" aria-controls="collapse<?=$itens?>">
                    <h4 class="mb-0 font-weight-bold btn">                    
                        <?php echo "$itens - $pergunta"; ?>
                    </h4>
                </div>
                <div id="collapse<?=$itens?>" class="collapse" aria-labelledby="item<?=$itens?>" data-parent="#accordion">
                    <div class="card-body">
                        <?= $resposta ?>
                    </div>
                </div>
            </div>
        <?php 
        ++$itens;
        endforeach;
        
        ?>
    </div>
</div>