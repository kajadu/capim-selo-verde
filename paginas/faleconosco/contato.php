<script src="assets/js/contato.js"></script>
<div class="container">
    <div class="alert bg-success text-light text-center" role="alert">
        <h2 class="font-weight-bold">CONTATO</h2> 
    </div>
    <div class="bg-light">
        <form class="ml-3" id="form-contato" name="form-contato">  
            <div class="row">
                <div class="form-group col-6 offset-3">
                    <label for="nome">Nome *</label>
                    <input class="form-control" type="text" id="nome" name="nome" oblig="1">
                </div>
            </div>
            <div class="row">
                <div class="form-group col-6 offset-3">
                    <label for="email">Email *</label>
                    <input class="form-control" type="email" id="email" name="email" oblig="1">
                    
                </div>
            </div>
            <div class="row">
                <div class="form-group col-6 offset-3">
                    <label for="msg">Mensagem</label>                
                    <textarea class="form-control" id="msg" name="msg" cols="30" rows="10"></textarea>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-6 offset-3">
                    <button type="button" id="enviar-contato" class="btn btn-success mt-4">Enviar</button>
                </div>
            </div>
        </form>
    </div>
</div>
