<div class="container">
    <div class="alert bg-success text-light text-center" role="alert">
        <h2 class="font-weight-bold">QUANDO USAR</h2> 
    </div>

    <div class="row">
        <div class="col-6">
            <img class="img-fluid" src="assets/img/quandousar/quandousar_festas.png" alt="Quando Usar Festas">
            <h2><b>Festas</b></h2>
            <p class="lead">Festas com temas e públicos diversificados utilizam o Capim EcoCopo como alternativa aos descartáveis. Faça Um evento sustentável que gera um consumo consciente e uma mudança de comportamento.</p>
            <p class="lead">Nos temos alguns serviços que podem te auxiliar:</p>
            <ul class="lead">
                <li>Consultoria para o planejamento</li>
                <li>Capim EcoCopo consignado</li>
                <li>Operação 360</li>
                <li>Gestão de resíduos</li>
                <li>Higienização</li>
            </ul>
        </div>
        <div class="col-6">
            <img class="img-fluid" src="assets/img/quandousar/quandousar_shows.png" alt="Quando Usar Shows">
            <h2><b>Shows e Festivais</b></h2>
            <p class="lead">Nada como ir assistir aquela banda que você ama e ter um evento limpo com o chão livre de descartaveis para pular a vontade e ate estender a canga pra sentar ne? E de quebra ainda leva oCapim EcoCopo personalizado pra casa como lembrança desse momento incrível!</p>
            <p class="lead">Nos temos alguns serviços que podem te auxiliar:</p>
            <ul class="lead">
                <li>Consultoria para o planejamento</li>
                <li>Capim EcoCopo consignado</li>
                <li>Operação evento lixo zero</li>
                <li>Gestão de resíduos</li>
                <li>Higienização</li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-6">
            <img class="img-fluid" src="assets/img/quandousar/quandousar_openbar.png" alt="Quando Usar Open Bar">
            <h2><b>Open Bar</b></h2>
            <p class="lead">Quando o evento é bebida liberada o ideal é servir tudo direto no Capim EcoCopo e assim evitar copo descartável no chão e desperdício de bebida. Sugerimos nosso Copim de 350ml + Porta copos.</p>
            <p class="lead">Nos temos alguns serviços que podem te auxiliar:</p>
            <ul class="lead">
                <li>Consultoria para o planejamento</li>
                <li>Capim EcoCopo consignado</li>
                <li>Operação evento lixo zero</li>
                <li>Gestão de resíduos</li>
                <li>Higienização</li>
            </ul>
        </div>
        <div class="col-6">
            <img class="img-fluid" src="assets/img/quandousar/quandousar_eventos_corp.png" alt="Quando Usar Eventos Corporativos">
            <h2><b>Eventos Corporativos e Congressos</b></h2>
            <p class="lead">Grandes empresas e corporações entendem e sabem o problema ambiental que causamos ao utilizarmos descartaveis. Ao promover encontros, brainstorms, palestras e debates use o Capim EcoCopo e asism fara um evento mais sustentável agregara valores poderosos a sua corporação. Nossos Porta-copos podem se transformar em porta-cracha e o Capim EcoCopo sera uma lembraca útil do seu evento para seus convidados.</p>            
            <p class="lead">Podemos auxiliar com o Serviço de Consultoria e planejamento.</p>
        </div>
    </div>
    <div class="row">
        <div class="col-6">
            <img class="img-fluid" src="assets/img/quandousar/quandousar_trabalho.png" alt="Quando Usar Adote Copo no Trabalho">
            <h2><b>Adote um Copo no Trabalho</b></h2>
            <p class="lead">Ao adotar um Capim EcoCopo no seu trabalho e dentro da sua empresa pode representar a economia de em média 700 copos descartáveis por ano além de estimular um consumo mais sustentável e uma mudança útil de comportamento dentro do ambiente de trabalho.</p>
            <p class="lead">Podemos auxiliar com o Serviço de Consultoria e planejamento.</p>            
        </div>
        <div class="col-6">
            <img class="img-fluid" src="assets/img/quandousar/quandousar_escolas_universidades.png" alt="Quando Usar Escolas Universidades">
            <h2><b>Escolas e Universidades</b></h2>
            <p class="lead">O estimulo de atos socioambientais sustentáveis em ambientes de ensino, culturais são muito importantes e agem diretamente no desenvolvimento de seres-humanos que serão responsáveis por verdadeiras mudanças. As cantinas e restaurantes dos escolas e universidades podem facilmente serem equipadas por copos reutilizáveis gerando menos lixo e menos impacto ambiental.</p>            
            <p class="lead">Nos temos alguns serviços que podem te auxiliar:</p>
            <ul class="lead">
                <li>Consultoria para o planejamento</li>
                <li>Capim EcoCopo consignado</li>
                <li>Gestão de resíduos</li>                
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-6">
            <img class="img-fluid" src="assets/img/quandousar/quandousar_eventos_esportivos.png" alt="Quando Usar Eventos Esportivos">
            <h2><b>Eventos Esportivos</b></h2>
            <p class="lead">Em Campeonatos estudantis, corridas e competições de rua, geralmente vemos milhares de descartáveis jogados pelo chão e muitos desses eventos são ao ar livre o que faz com que esse lixo va diretamente para a natureza. Trazemos uma solução para eventos desportivos que é capaz de eliminar 100% dos copos descartáveis criando um evento limpo e sustentável.</p>
            <p class="lead">Nos temos alguns serviços que podem te auxiliar:</p>
            <ul class="lead">
                <li>Consultoria para o planejamento</li>
                <li>Capim EcoCopo consignado</li>
                <li>Operação evento lixo zero</li>
                <li>Gestão de resíduos e higienização</li>
            </ul>          
        </div>
        <div class="col-6">
            <img class="img-fluid" src="assets/img/quandousar/quandousar_estadios_futebol.png" alt="Quando Usar Estadios Futebol">
            <h2><b>Estadios de Futebol</b></h2>
            <p class="lead">Seria lindo se todo mundo se esforçasse tanto pelo meio ambiente como se esforça para torcer pelo time do coração ne? Mas podemos unir os esforços e evitar lixo e descartáveis nos estádios de futebol! Coloque Capim EcoCopo nos bares personalizados com as bandeiras dos times e sirva todas as bebidas nele. Assim o estádio fica mais limpo, ajuda o planeta, é economicamente viável para o bar e o torcedor leva pra casa o copo do seu amado time!</p>            
        </div>
    </div>
    <div class="row">
        <div class="col-6">
            <img class="img-fluid" src="assets/img/quandousar/quandousar_festas_particulares.png" alt="Quando Usar Festas Particulares">
            <h2><b>Festas Particulares</b></h2>
            <p class="lead">Casamentos, festas de aniversário, formaturas… Você vai se surpreender com a redução de custo e ainda mais com o colorido do Copo Eco® nas suas fotos. Além disso, pode ser uma ótima lembrança daquele momento tão especial.</p>
        </div>
        <div class="col-6">
            <img class="img-fluid" src="assets/img/quandousar/quandousar_feiras_eventos_sociais.png" alt="Quando Usar Feiras/Eventos Sociais">
            <h2><b>Feiras e Eventos de Rua/Eventos Sociais</b></h2>
            <p class="lead">O Capim EcoCopo pode ser utilizado também em eventos de rua, praças e ações sociais. Presente nas feiras artesanais e nas barraquinhas ajudamos a manter a cidade limpa após os eventos.</p>
        </div>
    </div>
    <div class="row">
        <div class="col-6">
            <img class="img-fluid" src="assets/img/quandousar/quandousar_passear.png" alt="Quando Usar Leve para Passear">
            <h2><b>Leve seu Capim EcoCopo pra Passear</b></h2>
            <p class="lead">Vai para a praia? Academia, Buteco, parque ou trabalho? Adote um copo e leve o Capim EcoCopo junto! Com nossos porta Copos fica mais fácil ainda levar seu Capim EcoCopo na bolsa ou na mochila e assim evitar grande parte do lixo no dia-a-dia.</p>
        </div>
        <div class="col-6">            
        </div>
    </div>
</div>