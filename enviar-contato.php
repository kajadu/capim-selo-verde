<?php
ini_set( 'display_errors', '0');
require "./Libs/mail.php";
header('Content-Type: application/json');

$dados=$_POST;

$nome = strtoupper($dados['nome']);
$email = $dados['email'];
$mensagem = $dados['msg'];


$assunto = "FALE CONOSCO SITE CONTATO - NOME: ".$nome;
$msg = "
<!DOCTYPE html>
<html>
    <head>
        <style type='text/css'>
            body {
                background-color: #dbdbdb;
            }
            .bg-success{
                background-color: #165b2a;
            }
            
            .text-center{
                padding-top: 30px;
                padding-bottom: 30px;
                text-align: center;
            }
            .fixed-top {
                position: relative;
                margin-top: 4px;
                top:-22px;
            }
            .text-secondary {
                color: #6c757d;
            }
            .text-light {
                color: #f8f9fa;
            }
            .text-justify {
                text-align: justify;
            }
            .modal {
                position: absolute;
                top: 0;
                right: 0;
                bottom: 0;
                left: 0;
                z-index: 1050;
                margin-top: 70px;
                margin-bottom: 70px;
                margin-left: 70px;
                margin-right: 70px;
                overflow: h 0;
                box-shadow: 40px 40px 40px grey;
            }
            .modal-content {
                background-color: white;
            }
            .content{
                margin-left:70px;
                margin-right:70px;
            }
            .ml{
                margin-left:30px;
            }
            
            hr{
                box-sizing: content-box;
                height: 0;
                overflow: visible;
                margin-top: 1rem;
                margin-bottom: 1rem;
                border: 0;
                border-top: 1px solid rgba(0, 0, 0, 0.1);
            }
            .row {
                display: -ms-flexbox;
                display: flex;
                -ms-flex-wrap: wrap;
                flex-wrap: wrap;
                margin-right: -15px;
                margin-left: -15px;
            }
            .form-group {
                margin-bottom: 1rem;
                display: -ms-flexbox;
                display: flex;
                -ms-flex: 0 0 auto;
                flex: 0 0 auto;
                -ms-flex-flow: row wrap;
                flex-flow: row wrap;
                -ms-flex-align: center;
                align-items: center;
                margin-bottom: 0;
            }
            .form-control{
                padding: 0.375rem 0.75rem;
                font-size: 1rem;
                line-height: 1.5;
                color: #495057;
                border: 1px solid #ced4da;
                border-radius: 0.25rem;
                transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
            }
            .col-6 {
                -ms-flex: 0 0 50%;
                flex: 0 0 50%;            
                position: relative;
            }
            .col-4 {
                -ms-flex: 0 0 33.333333%;
                flex: 0 0 33.333333%;
                max-width: 33.333333%;
            }
            .font-weight-bold {
                font-weight: 700 !important;
            }
            
        </style>
    </head>
    <body>
        <div class='modal modal-content'>
            <div class='bg-success fixed-top'>            
                <h1 class='text-light text-center'>CONTATO ENVIADO PELO SITE</h1>            
            </div>
            <div class='content text-justify'>
                <div>
                    <h2 class='text-secondary'>CONTATO</h2>
                    <hr>
                    <div class='ml'>
                        <div class='row'>
                            <div class='form-group col-12'>
                                <label class='font-weight-bold text-secondary'><b>Nome: </b></label>
                                <input class='form-control ml' value='".$nome."' >
                            </div>                            
                        </div>
                        <div class='row'>
                            <div class='form-group col-6'>
                                <label class='font-weight-bold text-secondary'><b>E-mail: </b></label>
                                <input class='form-control ml' value='".$email."' >
                            </div>                        
                        </div>                        
                    </div>
                </div>
                <div>
                    <h2 class='text-secondary'>MENSAGEM</h2>
                    <hr>
                    <div class='ml'>
                        <div class='row'>
                            <div class='col-6'>
                                <p>".$mensagem."</p>
                                
                            </div>                                                    
                        </div>                        
                    </div>
                </div>
                
            </div>
                
        </div>
    </body>
</html>
";

$anexo="";
$para = "contato@capimseloverde.com.br";
//$para = "luis.felipe.cajado@hotmail.com";
$enviar_email = solicitarOrcamento($assunto,$msg, $anexo, $para);

if ($enviar_email == 1) {

    $result['msg']= "Muito bom ter você por aqui! 😊\n".
    "Juntos, somos todos parte da solução. ♻🌱.\n".
    "Até breve!";
    $result['error']= 0;
    
}else{

    $result['msgerror']= "Houve algum problema em nosso sistema já estamos analisando!";
    $result['error']= 1;
}

echo json_encode($result);


