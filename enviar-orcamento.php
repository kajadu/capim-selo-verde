<?php
ini_set( 'display_errors', '0');
require "./Libs/mail.php";
header('Content-Type: application/json');
$data_atual = date("Y-m-d");

$numero_solicitacao = "10-".str_replace("-","",$data_atual)."".rand(1000,9999);

$dados=$_POST;
$anexo=$_FILES;

$pagina = $dados['page'];
$qtdCoresImpressao = strtolower($dados['colors']);
$coresImpressao = strtolower($dados['cores']);
$quantidade = $dados['quantidade'];
$nome = strtolower($dados['nome']);
$email = $dados['email'];
$confirma_email = $dados['confirma-email'];
$celular = $dados['celular'];
$cep = $dados['cep'];
$endereco = strtolower($dados['endereco']);
$numero = $dados['numero'];
$bairro = strtolower($dados['bairro']);
$estado = strtolower($dados['estado']);
$complemento = $dados['complemento'];
$data_de_entrega = $dados['data-de-entrega'];
$cor_do_copo = $dados['cor-do-copo'];
$tipo_do_copo = $dados['tipo-do-copo'];
$porta_copos = $dados['porta-copos'];
$comentarios = strtolower($dados['comentarios']);


if(in_array("on", $dados)){
    $equipe_design = "sim";
}else{
    $equipe_design = "não";
}

if (empty($qtdCoresImpressao)) {
    $qtdCoresImpressao = "";
}

if ($porta_copos == "S") {
    $porta_copos = "sim";
}else{
    $porta_copos = "não";
}
if (!empty($_FILES['anexar']['name'])) {
    $arte_pronta = "sim";    
}else{
    $arte_pronta = "não";    
}
if(!empty($cor_do_copo)){
    $cor_do_copo = strtolower(str_replace("_"," ",$cor_do_copo));
}else{
    $cor_do_copo = "";
}

if($tipo_do_copo == "350ml"){
    $qtdCoresImpressao = "1";
}

$assunto = "ORÇAMENTO - Data Entrega: ".$data_de_entrega." - Cliente: ".$nome." - N: ".$numero_solicitacao;
$msg = "
<!DOCTYPE html>
<html>
    <head>
        <style type='text/css'>
            body {
                background-color: #dbdbdb;
            }
            .bg-success{
                background-color: #165b2a;
            }
            
            .text-center{
                padding-top: 30px;
                padding-bottom: 30px;
                text-align: center;
            }
            .fixed-top {
                position: relative;
                margin-top: 4px;
                top:-22px;
            }
            .text-secondary {
                color: #6c757d;
            }
            .text-light {
                color: #f8f9fa;
            }
            .text-justify {
                text-align: justify;
            }
            .modal {
                position: absolute;
                top: 0;
                right: 0;
                bottom: 0;
                left: 0;
                z-index: 1050;
                margin-top: 70px;
                margin-bottom: 70px;
                margin-left: 70px;
                margin-right: 70px;
                overflow: h 0;
                box-shadow: 40px 40px 40px grey;
            }
            .modal-content {
                background-color: white;
            }
            .content{
                margin-left:70px;
                margin-right:70px;
            }
            .ml{
                margin-left:30px;
            }
            
            hr{
                box-sizing: content-box;
                height: 0;
                overflow: visible;
                margin-top: 1rem;
                margin-bottom: 1rem;
                border: 0;
                border-top: 1px solid rgba(0, 0, 0, 0.1);
            }
            .row {
                display: -ms-flexbox;
                display: flex;
                -ms-flex-wrap: wrap;
                flex-wrap: wrap;
                margin-right: -15px;
                margin-left: -15px;
            }
            .form-group {
                margin-bottom: 1rem;
                display: -ms-flexbox;
                display: flex;
                -ms-flex: 0 0 auto;
                flex: 0 0 auto;
                -ms-flex-flow: row wrap;
                flex-flow: row wrap;
                -ms-flex-align: center;
                align-items: center;
                margin-bottom: 0;
            }
            .form-control{
                padding: 0.375rem 0.75rem;
                font-size: 1rem;
                line-height: 1.5;
                color: #495057;
                border: 1px solid #ced4da;
                border-radius: 0.25rem;
                transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
            }
            .col-6 {
                -ms-flex: 0 0 50%;
                flex: 0 0 50%;            
                position: relative;
            }
            .col-4 {
                -ms-flex: 0 0 33.333333%;
                flex: 0 0 33.333333%;
                max-width: 33.333333%;
            }
            .font-weight-bold {
                font-weight: 700 !important;
            }
            
        </style>
    </head>
    <body>
        <div class='modal modal-content'>
            <div class='bg-success fixed-top'>            
                <h1 class='text-light text-center'>Solicitação de Orçamento Nº ".$numero_solicitacao."</h1>
            </div>
            <div class='content text-justify'>
                <div>
                    <div class='ml'>
                        <label class='font-weight-bold text-secondary'><b>Nome: </b></label>
                        <label class='font-weight-bold text-secondary'>".$nome."</label><br>
                    
                        <label class='font-weight-bold text-secondary'><b>E-mail: </b></label>
                        <label class='font-weight-bold text-secondary'>".$email."</label><br>
                        
                        <label class='font-weight-bold text-secondary'><b>Celular/Whatsapp: </b></label>
                        <label class='font-weight-bold text-secondary'>".$celular."</label>
                        <br><br>


                        <label class='font-weight-bold text-secondary'><b>Tipo do Capim EcoCopo: </b></label>
                        <label class='font-weight-bold text-secondary'>".$tipo_do_copo."</label><br>

                        <label class='font-weight-bold text-secondary'><b>Cor do Capim EcoCopo: </b></label>
                        <label class='font-weight-bold text-secondary'>".$cor_do_copo."</label><br>

                        <label class='font-weight-bold text-secondary'><b>Quantidade de cores de Impressão: </b></label>
                        <label class='font-weight-bold text-secondary'>".$qtdCoresImpressao."</label><br>

                        <label class='font-weight-bold text-secondary'><b>Cor/cores: </b></label>
                        <label class='font-weight-bold text-secondary'>".$coresImpressao."</label><br>

                        <label class='font-weight-bold  text-secondary'><b>Quantidade: </b></label>
                        <label class='font-weight-bold text-secondary'>".$quantidade."</label>
                        <br><br>
                        
                        <label class='font-weight-bold text-secondary'><b>Endereço: </b></label>
                        <label class='font-weight-bold text-secondary'>".$endereco.", ".$numero." - ".$complemento." - ".$bairro." - ".$estado." - ".$cep."</label><br>

                        <label class='font-weight-bold text-secondary'><b>Data de entrega que o cliente precisa: </b></label>
                        <label class='font-weight-bold text-secondary'>".$data_de_entrega."</label>
                        <br><br>
                            
                        
                        <label class='font-weight-bold text-secondary'><b>Precisa do serviço de criação de arte: </b></label>
                        <label class='font-weight-bold text-secondary'>".$equipe_design."</label><br>
        
                        <label class='font-weight-bold text-secondary'><b>Incluir porta copos: </b></label>
                        <label class='font-weight-bold text-secondary'>".$porta_copos."</label><br>
        
                        <label class='font-weight-bold text-secondary'><b>Arte pronta: </b></label>
                        <label class='font-weight-bold text-secondary'>".$arte_pronta."</label><br>
        
                        <label class='font-weight-bold text-secondary'><b>Comentarios: </b></label><br>
                        <spam class='text-secondary'>".$comentarios."</spam><br>
                        
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
";


$para = "contato@capimseloverde.com.br";
//$para = "luis.felipe.cajado@hotmail.com";
//$para = "luis.cajadosk8@gmail.com";
//$para="leordgsts@gmail.com";

$enviar_email = solicitarOrcamento($assunto,$msg, $anexo, $para);

if ($enviar_email == 1) {

    $result['msg']= "Parabéns por sua iniciativa de consumo consciente. ♻ \n".
    "É muito bom ter você por aqui 😊. \n".
    "Acreditamos que juntos somos todos parte da solução! \n".
    "Em breve responderemos seu e-mail, detalhando o passo a passo para concretizar seu pedido.";
    $result['erro']=0;
    
    
}else{

    $result['msgerror']= "Houve algum problema na solicitação desse orçameto!";
    $result['erro']=1;
    
}

echo json_encode($result);
