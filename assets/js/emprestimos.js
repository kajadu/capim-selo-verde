$(function () {
    // $("#outras_quantidades").blur(function(){
    //     var quant = $("#outras_quantidades").val()
    //     if (quant < 100){
    //         $("#outras_quantidades").prop("style=border-color: 5px, solid, red;");
    //         alert("Quantidade minima 100");

    //     }
    //     if(!quant == ""){
    //         $("#quantidade").val("");
    //     }
    //     var quantidade1 = $("#quantidade").val();
    //     var quantidade2 = $("#outras_quantidades").val();
    //     //console.log(quantidade1, quantidade2);        
    // });

    $("#cep").mask("00000-000");
    $("#celular").mask("(00) 0 0000-0000");
    // $("#datepicker").datepicker({
    //     showOtherMonths: true,
    //     selectOtherMonths: true,
    //     dateFormat: 'dd/mm/yy',
    //     dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
    //     dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
    //     dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
    //     monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
    //     monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],        
    // });

    $("#cep").on('focusout',function(){
        
        $.ajax({
            url: "buscar-endereco.php",
            data: {cep:$(this).val()},
            method: "POST",
            dataType: "json",
            
            success: function(data, item)
            {
                console.log(data.logradouro);
                $("#endereco").val(data.logradouro);
                $("#bairro").val(data.bairro);
                $("#estado").val(data.uf);
                
            }
        });        
    });

    $("#email").on('focusout',function(){
        var email_validar = $(this).val();
        if (!IsEmail(email_validar)){
            $(this).addClass("border-danger");
            $("#solicitar-emprestimo").attr("disabled",true);
            
            // alert("Favor informar um email válido");
            
        }else{
            $("#solicitar-emprestimo").attr("disabled",false);
            
        }
        
    });

    $("#confirma-email").on('focusout',function(){
        var email_validar = $(this).val();        
        if (!IsEmail(email_validar)){
            $(this).addClass("border-danger");
            $("#solicitar-emprestimo").attr("disabled",true);
            
            // alert("Favor informar um email válido");
        }else{
            $("#solicitar-emprestimo").attr("disabled",false);
            
        }
    });
    

    var campos = $("input");
    
    $.each(campos, function(i , campo){

        if ($("#"+campo.id).attr("oblig") == "1") {

            $("#"+campo.id).on('focus',function () {
               
                $("#"+campo.id).removeClass("border-danger");
                $("#solicitar-emprestimo").attr("disabled",false);
               
            });
            $("#"+campo.id).on('focusout',function () {
                if(campo.value == ""){
                    $("#"+campo.id).addClass("border-danger");
                    
                }else{
                    $("#"+campo.id).removeClass("border-danger");
                    
                }                  
            });
        }
    });
    
    $("#anexar").change(function () {

        if($(this).val != 0){            
            var valor = $(this).val();
            
            valor = valor.split("\\")
            valor = valor.slice(-1)            
            $("#label-anexar").text(valor);
        }
        
    });
    
    $("#solicitar-emprestimo").click(function(){
        $(this).attr("disabled",true);
        var dados = $("input");
        var erromsg = "";
        var erro = 0;
        
        $.each(dados, function(i , dado){            
            
            if (($("#"+dado.id).attr("oblig") == "1") && (dado.value=="")) {
                
                $("#"+dado.id).addClass("border-danger");
                erro = 1;
                erromsg = "Favor preencher os campos obrigatórios !";
            }

            if (dado.name == "confirma-email") {
                var email = $("#email").val()
                if($("#"+dado.id).val()!=email){
                    $("#"+dado.id).addClass("border-danger");
                    erro = 1;
                    erromsg = "Favor informar os emails iguais!";
                }
            }
            
        });
        
        if (erro == 1) {
            
            alert(erromsg);
            return false;
        }
        
        $.ajax({
            url: "enviar-emprestimo.php",
            data: $("#form-emprestimo").serialize(),
            method: "POST",
            dataType: "json",
            complete: function(r){
                if (r.responseJSON.error == 1){
                    alert(r.responseJSON.msgerror);
                }else{
                    alert(r.responseJSON.msg);
                    location.reload();
                }                
            }        
        })
    });
    function IsEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }
});
