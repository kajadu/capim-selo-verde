$(function () {
    

    var campos = $("input");
    
    $.each(campos, function(i , campo){

        if ($("#"+campo.id).attr("oblig") == "1") {

            $("#"+campo.id).on('focus',function () {
               
                $("#"+campo.id).removeClass("border-danger");
                $("#enviar-contato").attr("disabled",false);                              
               
            });
            $("#"+campo.id).on('focusout',function () {
                
                if(campo.value == ""){
                    $("#"+campo.id).addClass("border-danger");
                    
                    
                }else{
                    $("#"+campo.id).removeClass("border-danger");
                    
                    
                }                  
            });
        }
    });
    $("#email").on('focusout',function(){
        var email_validar = $(this).val();
        if (!IsEmail(email_validar)){
            $(this).addClass("border-danger");
            $("#enviar-contato").attr("disabled",true);
            alert("Favor informar um email válido");
            
        }else{
            $("#enviar-contato").attr("disabled",false);
        }
        
    });
    $("#enviar-contato").click(function(){
        $(this).attr("disabled",true);
        
        var dados = $("input");
        var erromsg = "Favor preencher os campos obrigatórios !";
        var erro = 0;
        
        $.each(dados, function(i , dado){            
            
            if (($("#"+dado.id).attr("oblig") == "1") && (dado.value=="")) {
                
                $("#"+dado.id).addClass("border-danger");
                
                erro = 1;
            }
        });
        
        if (erro == 1) {
            $("#enviar-contato").attr("disabled",true);
            alert(erromsg);
            return false;
        }        
        
        $.ajax({
            url: "enviar-contato.php",
            data: $("#form-contato").serialize(),
            method: "POST",
            dataType: "json",
            
            complete: function(r)
            {
                if (r.responseJSON.error == 1){
                    alert(r.responseJSON.msgerror);
                }else{
                    alert(r.responseJSON.msg);
                }
                $("#enviar-contato").attr("disabled",false);
            }
        }); 
        
    });
    function IsEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }
});