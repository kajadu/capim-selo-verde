<?php
ini_set('display_errors','1');
require 'templates/header.php';
require "Libs/funcs.php";

?>
        <div>
            <div class="row">
                <div class="col-12">
                    <?php
                    $get = isset($_GET['p']) ? $_GET['p'] : '';
                    require 'templates/menu.php';
                    ?>
                </div>
            </div>
        
            <div class="row">
                <div class="container-fluid bodypage col-12">
                    <?php
                    rotas($get);
                    ?>
                </div>    
            </div>
            <div class="row">
                <div class="col-12">
                    <?php require 'templates/footer.php'; ?>
                </div>
            </div>
        </div>
    </body>
</html>