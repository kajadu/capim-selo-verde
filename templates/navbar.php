<?php
$stylemenu = " btn btn-menu ";
$stylebgdropdown = "";
$styletextdropdown = " text-menu ";
?>

<div class="d-flex px-md-1 bg-white">

    
    <div class="ml-lg-5 logo2">
        <a href="?p=home"><img class="logo" src="assets/img/capim-logo.png" alt=""/></a>
    </div>
    
    <nav class="md-sm-auto mt-3 navbar-expand-sm navbar-left">
        <div class="navbar-header">
            <button class="navbar-toggler <?php echo $stylemenu ?>" type="button" data-toggle="collapse" data-target="#links-menu" aria-controls="links-menu" aria-expanded="false" aria-label="Toggle navigation">
                <i class="material-icons"> menu</i>
            </button>
        </div>

        <div class="collapse navbar-collapse" id="links-menu">
            <ul class="nav nav-pills mb-3">

                <li class="nav-item">
                    <a class="btn-menu
                    <?php
                    echo $stylemenu;
                    echo active($get);
                    ?>"  href="?p=home" >Quem Somos</a>
                </li>

                <li class="nav-item dropdown">
                    <a id="orcamentos" class="btn-menu dropdown-toggle <?php
                    echo $stylemenu;
                    echo active($get, 'compra');
                    echo active($get, 'consignado');                    
                    ?>" data-toggle="dropdown" role="button" aria-haspopup="true" href="#" aria-expanded="false">Orçamento</a>
                    <div class="dropdown-menu <?php echo $stylebgdropdown ?>" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item <?php echo $styletextdropdown ?>" href="?p=compra">Compra</a>
                        <a class="dropdown-item <?php echo $styletextdropdown ?>" href="?p=emprestimo">Emprestimo</a>
                    </div>
                </li>              
                <!-- <li class="nav-item">
                    <a class="btn-menu
                    <?php
                    // echo $stylemenu;
                    // echo active($get, 'quemsomos');
                    ?>"  href="?p=quemsomos" >Quem Somos</a>
                </li> -->

                <li class="nav-item dropdown" >
                    <a id="produtos" class=" btn-menu dropdown-toggle <?php
                    echo $stylemenu;
                    echo active($get, 'ecocopo550ml');
                    echo active($get, 'ecocopo500ml');                    
                    echo active($get, 'ecocopo400ml');                    
                    echo active($get, 'ecocopo350ml');                    
                    echo active($get, 'longdrink330ml');                    
                    echo active($get, 'champanhe');                    
                    echo active($get, 'gin');                    
                    echo active($get, 'portacopos');
                    echo active($get, 'comopersonalizar');
                    ?>" data-toggle="dropdown" aria-haspopup="true" href="#" aria-expanded="false">Produtos</a>
                    <div class="dropdown-menu <?php echo $stylebgdropdown ?>" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item <?php echo $styletextdropdown ?>" href="?p=ecocopo550ml">CAPIM ECOCOPO 550ML</a>
                        <!-- <a class="dropdown-item <?php echo $styletextdropdown ?>" href="?p=ecocopo500ml">CAPIM ECOCOPO 500ML</a> -->
                        <a class="dropdown-item <?php echo $styletextdropdown ?>" href="?p=ecocopo400ml">CAPIM ECOCOPO 400ML</a>
                        <a class="dropdown-item <?php echo $styletextdropdown ?>" href="?p=ecocopo350ml">CAPIM ECOCOPO 350ML</a>
                        <!-- <a class="dropdown-item <?php echo $styletextdropdown ?>" href="?p=longdrink330ml">LONGDRINK 330ML</a>
                        <a class="dropdown-item <?php echo $styletextdropdown ?>" href="?p=champanhe">TAÇA CHAMPANHE</a>
                        <a class="dropdown-item <?php echo $styletextdropdown ?>" href="?p=gin"> TAÇA GIN</a>
                        <a class="dropdown-item <?php echo $styletextdropdown ?>" href="?p=portacopos">PORTA COPOS</a> -->
                        <a class="dropdown-item <?php echo $styletextdropdown ?>" href="?p=comopersonalizar">COMO PERSONALIZAR</a>
                    </div>
                </li>

                <!-- <li class="nav-item dropdown">
                    <a id="servicos" class="btn-menu dropdown-toggle <?php
                    // echo $stylemenu;
                    // echo active($get, 'consultoriaplan');
                    // echo active($get, 'comunicacao');
                    // echo active($get, 'operacaoeventos');
                    // echo active($get, 'gestaoresiduos');
                    // echo active($get, 'higienizacao');
                    // echo active($get, 'operacao360');
                    // echo active($get, 'eventozerolixo');
                    ?>" data-toggle="dropdown" role="button" aria-haspopup="true" href="#" aria-expanded="false">Serviços</a>
                    <div class="dropdown-menu <?php echo $stylebgdropdown ?>" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item <?php echo $styletextdropdown ?>" href="?p=consultoriaplan">Consultoria e Planejamento</a>
                        <a class="dropdown-item <?php echo $styletextdropdown ?>" href="?p=comunicacao">Comunicação Previa e Online</a>
                        <a class="dropdown-item <?php echo $styletextdropdown ?>" href="?p=operacaoeventos">Operação em Eventos</a>
                        <a class="dropdown-item <?php echo $styletextdropdown ?>" href="?p=gestaoresiduos">Gestão de Resíduos</a>
                        <a class="dropdown-item <?php echo $styletextdropdown ?>" href="?p=higienizacao">Higienização</a>
                        <a class="dropdown-item <?php echo $styletextdropdown ?>" href="?p=operacao360">Operação Ação 360</a>
                        <a class="dropdown-item <?php echo $styletextdropdown ?>" href="?p=eventozerolixo">EVENTO ZERO LIXO</a>
                    </div>
                </li> -->
                
                <li class="nav-item">
                    <a class="btn-menu <?php
                    echo active($get, 'conceitocaucao');
                    echo $stylemenu;
                    ?>" id=""  href="?p=conceitocaucao">Conceito Caução</a>
                </li>
                
                <!-- <li class="nav-item">
                    <a class="btn-menu <?php
                    // echo active($get, 'quandousar');
                    // echo $stylemenu;
                    ?>" id=""  href="?p=quandousar">Quando Usar</a>
                </li>                 -->

                <!-- <li class="nav-item">
                    <a class="btn-menu <?php
                    // echo active($get, 'ondeachar');
                    // echo $stylemenu;
                    ?>" id=""  href="?p=ondeachar">Onde Achar</a>
                </li> -->


                <li class="nav-item dropdown ">
                    <a class="btn-menu dropdown-toggle <?php
                    echo $stylemenu;
                    echo active($get, 'faq');
                    echo active($get, 'contato');
                    ?>" data-toggle="dropdown" role="button" aria-haspopup="true" href="#" aria-expanded="false">Fale Conosco</a>
                    <div class="dropdown-menu <?php echo $stylebgdropdown ?>" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item <?php echo $styletextdropdown ?>" href="?p=faq">Perguntas Frequentes</a>
                        <a class="dropdown-item <?php echo $styletextdropdown ?>" href="?p=contato">Contato</a>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
</div>