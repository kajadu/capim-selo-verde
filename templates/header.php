<!doctype html>
<html lang="pt">
    <head>
        
        <title>Capim Eco Copo - <?php echo strtoupper($_GET['p']) ?></title>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="Eco Copo 550ml, Copo ecológico 400ml, Copo reutilizável 350ml, copo durável 500ml, Copo retornável,  copo retrátil 320ml, copo biodegradável 300mll, copo compostável 250ml, livre de BPA, polipropileno (PP), personalizados e 100% recicláveis. Eventos sustentáveis, marketing promocional, Lixo zero.">
        <meta name="robots" content="Evento sustentável, Copo eco, meucopo eco, eco copo, conceito caução, copo retornável, copo dobrável.">
        <meta name="googlebot" content="Evento sustentável, Copo eco, meucopo eco, eco copo, conceito caução, copo retornável, copo dobrável.">
        <meta name="author" content="Capim Eco Copo">


        
        <!--        CSS-->
        <link href="assets/css/style.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/buttonscolors.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/jquery-ui.theme.css" rel="stylesheet" type="text/css"/>

        
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
        

        <!--        Java script-->
        <script src="assets/js/jquery-ui.min.js" type="text/javascript"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="assets/js/bootstrap.bundle.min.js" type="text/javascript"></script>
        <script src="assets/js/menu.js" type="text/javascript"></script>
        <script src="assets/js/jquery.mask.min.js" type="text/javascript"></script>
        
        

        <div id="fb-root"></div>
        <script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v3.2';
        fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>

        
    </head>

    <body>        