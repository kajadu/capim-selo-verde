<?php
$stylemenu = " text-dark ";
$stylebgdropright = "";
$styletextdropright = " text-left ";
?>

<div class="d-flex d-row justify-content-center bg-white">
   
    <nav class="mt-2 text-left">
        <ul class="nav flex-column nav-pills mb-3">
            <li class="nav-item">
                <a class=" 
                <?php
                echo $stylemenu;
                ?>"  href="?p=home" >Quem Somos</a>
            </li>

            <li class="nav-item dropright">
                <a id="orcamentos-mapa" class="dropdown-toggle <?php
                echo $stylemenu;
                ?>" data-toggle="dropdown" role="button" aria-haspopup="true" href="#" aria-expanded="false">Orçamento</a>
                <div class="dropdown-menu <?php echo $stylebgdropright ?>" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item <?php echo $styletextdropright ?>" href="?p=compra">Compra</a>
                    <a class="dropdown-item <?php echo $styletextdropright ?>" href="?p=emprestimo">Emprestimo</a>
                </div>
            </li> 

            <li class="nav-item dropright">
                <a id="produtos-mapa" class="dropdown-toggle <?php
                echo $stylemenu;
                ?>" data-toggle="dropdown" role="button" aria-haspopup="true" href="#" aria-expanded="false">Produtos</a>
                <div class="dropdown-menu <?php echo $stylebgdropright ?>" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item <?php echo $styletextdropright ?>" href="?p=ecocopo550ml">CAPIM ECOCOPO 550ML</a>
                    <!-- <a class="dropdown-item <?php echo $styletextdropright ?>" href="?p=ecocopo500ml">CAPIM ECOCOPO 500ML</a> -->
                    <a class="dropdown-item <?php echo $styletextdropright ?>" href="?p=ecocopo400ml">CAPIM ECOCOPO 400ML</a>
                    <a class="dropdown-item <?php echo $styletextdropright ?>" href="?p=ecocopo350ml">CAPIM ECOCOPO 350ML</a>
                    <!-- <a class="dropdown-item <?php echo $styletextdropright ?>" href="?p=longdrink330ml">LONGDRINK 330ML</a>
                    <a class="dropdown-item <?php echo $styletextdropright ?>" href="?p=champanhe">TAÇA CHAMPANHE</a>
                    <a class="dropdown-item <?php echo $styletextdropright ?>" href="?p=gin"> TAÇA GIN</a>
                    <a class="dropdown-item <?php echo $styletextdropright ?>" href="?p=portacopos">PORTA COPOS</a> -->
                    <a class="dropdown-item <?php echo $styletextdropright ?>" href="?p=comopersonalizar">COMO PERSONALIZAR</a>
                </div>
            </li>


            <!-- <li class="nav-item dropright">
                <a id="servicos-mapa" class="dropdown-toggle <?php
                echo $stylemenu;
                ?>" data-toggle="dropdown" role="button" aria-haspopup="true" href="#" aria-expanded="false">Serviços</a>
                <div class="dropdown-menu <?php echo $stylebgdropright ?>" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item <?php echo $styletextdropright ?>" href="?p=consultoriaplan">Consultoria e Planejamento</a>
                    <a class="dropdown-item <?php echo $styletextdropright ?>" href="?p=comunicacao">Comunicação Previa e Online</a>
                    <a class="dropdown-item <?php echo $styletextdropright ?>" href="?p=operacaoeventos">Operação em Eventos</a>
                    <a class="dropdown-item <?php echo $styletextdropright ?>" href="?p=gestaoresiduos">Gestão de Resíduos</a>
                    <a class="dropdown-item <?php echo $styletextdropright ?>" href="?p=higienizacao">Higienização</a>
                    <a class="dropdown-item <?php echo $styletextdropright ?>" href="?p=operacao360">Operação Ação 360</a>
                    <a class="dropdown-item <?php echo $styletextdropright ?>" href="?p=eventozerolixo">EVENTO ZERO LIXO</a>
                </div>
            </li> -->
            
            <li class="nav-item">
                <a class=" <?php echo $stylemenu; ?> " id=""  href="?p=conceitocaucao">Conceito Caução</a>
            </li>
            
            <!-- <li class="nav-item">
                <a class=" <?php echo $stylemenu; ?> " id=""  href="?p=quandousar">Quando Usar</a>
            </li>-->

            <!-- <li class="nav-item">
                <a class=" <?php echo $stylemenu; ?> " id=""  href="?p=ondeachar">Onde Achar</a>
            </li> -->


            <li class="nav-item dropright ">
                <a class="  dropdown-toggle <?php
                echo $stylemenu;
                ?>" data-toggle="dropdown" role="button" aria-haspopup="true" href="#" aria-expanded="false">Fale Conosco</a>
                <div class="dropdown-menu <?php echo $stylebgdropright ?>" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item <?php echo $styletextdropright ?>" href="?p=faq">Perguntas Frequentes</a>
                    <a class="dropdown-item <?php echo $styletextdropright ?>" href="?p=contato">Contato</a>
                </div>
            </li>
        </ul>
        
    </nav>
</div>