<?php

include 'Libs/PHPMailer/src/Exception.php';
include 'Libs/PHPMailer/src/PHPMailer.php';
include 'Libs/PHPMailer/src/SMTP.php';

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

function solicitarOrcamento($assunto, $msg, $anexo=[], $para){

    $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
    try {
        
        //Server settings
        //$mail->SMTPDebug = 2;                                 // Enable verbose debug output
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'capimseloverde.com.br';  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = 'orcamento@capimseloverde.com.br';                 // SMTP username
        $mail->Password = 'k09bQh@SOqFb';                           // SMTP password
        $mail->SMTPSecure = 'ssl/tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587;                                    // TCP port to connect to
        $mail->CharSet = "UTF-8";

        //$mail->CharSet = 'UTF-8';
        //Recipients
        $mail->setFrom('orcamento@capimseloverde.com.br', 'Capim-Selo-Verde');
        //$mail->addAddress('leordgsts@gmail.com');     // Add a recipient
        $mail->addAddress($para);     // Add a recipient

        //$mail->addAddress('ellen@example.com');               // Name is optional
        //$mail->addReplyTo('info@example.com', 'Information');
        //$mail->addCC('luis.gabriel@allink.com.br');
        //$mail->addBCC('bcc@example.com');
        
        //Attachments
        if (!empty($anexo['file']['name'])){
            $mail->addAttachment($anexo["file"]["tmp_name"],$anexo["file"]["name"]);         // Add attachments
        }
        
        
        //Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = "$assunto";
        $mail->Body    = "$msg";
        //$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
        
        $mail->send();
        $status=1;

        return $status;
    } catch (Exception $e) {
        $status=0;
        //return 'Message could not be sent. Mailer Error: '. $mail->ErrorInfo;
        return $status;
    }

}    
