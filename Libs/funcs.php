<?php

include "Libs/PHPMailer/src/OAuth.php";
include "Libs/PHPMailer/src/PHPMailer.php";
include "Libs/PHPMailer/src/POP3.php";
include "Libs/PHPMailer/src/SMTP.php";

function rotas($pagina) {
    switch ($pagina) {

        case'quemsomos':
            require "paginas/quemsomos/$pagina.php";
            break;

        case 'produtos':
            require "paginas/produtos/$pagina.php";
            break;
        case 'ecocopo550ml':
            require "paginas/produtos/tipos/$pagina/$pagina.php";
            break;
        case 'ecocopo500ml':
            require "paginas/produtos/tipos/$pagina/$pagina.php";
            break;
        case 'ecocopo400ml':
            require "paginas/produtos/tipos/$pagina/$pagina.php";
            break;
        case 'ecocopo350ml':
            require "paginas/produtos/tipos/$pagina/$pagina.php";
            break;
        case 'longdrink330ml':
            require "paginas/produtos/tipos/$pagina/$pagina.php";
            break;
        case 'champanhe':
            require "paginas/produtos/tipos/$pagina/$pagina.php";
            break;
        case 'gin':
            require "paginas/produtos/tipos/$pagina/$pagina.php";
            break;
        case 'portacopos':
            require "paginas/produtos/tipos/$pagina/$pagina.php";
            break;      
        case 'comopersonalizar':
            require "paginas/produtos/personalizacao/$pagina.php";
            break;
        
        case 'servicos':
            require "paginas/servicos/$pagina.php";
            break;
        case 'consultoriaplan':
            require "paginas/servicos/$pagina.php";
            break;
        case 'comunicacao':
            require "paginas/servicos/$pagina.php";
            break;
        case 'operacaoeventos':
            require "paginas/servicos/$pagina.php";
            break;
        case 'gestaoresiduos':
            require "paginas/servicos/$pagina.php";
            break;
        case 'higienizacao':
            require "paginas/servicos/$pagina.php";
            break;
        case 'operacao360':
            require "paginas/servicos/$pagina.php";
            break;
        case 'eventozerolixo':
            require "paginas/servicos/$pagina.php";
            break;

        case 'conceitocaucao':
            require "paginas/conceitocaucao/$pagina.php";
            break;

        case 'quandousar':
            require "paginas/quandousar/$pagina.php";
            break;

        case 'ondeachar':
            require "paginas/ondeachar/$pagina.php";
            break;
        
        
        case 'orcamentos':
            require "paginas/orcamentos/$pagina.php";
            break;
        case 'compra':
            require "paginas/orcamentos/$pagina.php";
        break;
        case 'emprestimo':
            require "paginas/orcamentos/$pagina.php";
        break;

        case 'contato':
            require "paginas/faleconosco/$pagina.php";
            break;
        case 'faq':
            require "paginas/faleconosco/$pagina.php";
            break;
        
       
             
        default :
            require 'paginas/home.php';
    }
}


function active($pagina, $link = '') {
    if ($pagina == $link) {
        return 'active';
    }
    return '';
}

function myUrlEncode($string) {
    $entities = array('%21', '%2A', '%27', '%28', '%29', '%3B', '%3A', '%40', '%26', '%3D', '%2B', '%24', '%2C', '%2F', '%3F', '%25', '%23', '%5B', '%5D');
    $replacements = array('!', '*', "'", "(", ")", ";", ":", "@", "&", "=", "+", "$", ",", "/", "?", "%", "#", "[", "]");
    return str_replace($entities, $replacements, urlencode($string));
}

function formata_reais($valor) {

    return 'RS ' . number_format($valor, 2, ',', '.');
}

function formatNumber($number) {
    $chars = ['.', '-', '(', ')', '/'];
    $newnumber = str_replace($chars, '', $number);
    return $newnumber;
}

function verificaCPF($cpf) {
    $cpf2 = formatNumber($cpf);
    $cut = substr($cpf2, 0, -2);
    $digito = substr($cpf2, -2);
    $tam = strlen($cut);

    $sumd1 = [];
    $sumd2 = [];
    for ($i = 0; $i < $tam; $i++) {
        $n = $i + 1;
        $chars = substr($cut, $i, 1);
        $sumd1 [$i] = $chars * $n;
        $sumd2 [$i] = $chars * $i;
    }

    $d1 = array_sum($sumd1); // 232
    $d2 = array_sum($sumd2) + ($d1 * 9); //2277

    $rd1 = valorD($d1); //1
    $rd2 = valorD($d2); // 10 -> 0

    if ($rd1 == substr($digito, 0, -1) && ($rd2 == substr($digito, 1))) {
        return TRUE;
    } else {
        return FALSE;
    }
}
function dataValida($dataprospec){
    if (strtotime($dataprospec)<strtotime(date("d-m-Y"))) {
        return FALSE;
    } else{
        return TRUE;
    }   
}

function enviaEmail($assunto, $msg){
    $mail = new PHPMailer();
    $mail->IsSMTP();           // Set mailer to use SMTP
    try {
        $mail->Host = 'capimseloverde.com.br';  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Port = 465;                                    // TCP port to connect to
        $mail->SMTPSecure = 'tls';
        $mail->Username = 'orcamento@capimseloverde.com.br';                 // SMTP username
        $mail->Password = 'k09bQh@SOqFb';                           // SMTP password
        $mail->CharSet = "UTF-8";
        //Define o remetente
        $mail->setFrom('orcamento@capimseloverde.com.br', 'Mailer');
        $mail->Subject = $assunto; //Assunto do e-mail
        //Define os destinatário(s)
        $mail->addAddress('luis.felipe.cajado@hotmail.com');
        $mail->addCC('luis.gabriel@allink.com.br');
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Body = $msg;
        //$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
        $mail->Send();
        // Limpa os destinatários e os anexos
        $mail->ClearAllRecipients();
        $mail->ClearAttachments();
        
        return TRUE;
        
        
    } catch (Exception $e) {
        
        return FALSE;

    }
}